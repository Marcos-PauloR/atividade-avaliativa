package com.example.atividadeavaliativa;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class TelaInvertida extends AppCompatActivity {
    TextView txt;
    Button btn;
    Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_invertida);

        txt = findViewById(R.id.PalavraInvertida);
        btn = findViewById(R.id.btnVoltaInverte);

        txt.setText(getIntent().getStringExtra("PalavraInvertida"));

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(TelaInvertida.this, Inversor.class);
                startActivity(i);
                finish();
            }
        });



    }
}