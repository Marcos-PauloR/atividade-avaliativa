package com.example.atividadeavaliativa;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

public class Secreto extends AppCompatActivity {

    TextView codigo, testeSecreto;
    EditText texto;
    Button btnCodigo, btnVolta;
    ListView listView;
    ArrayAdapter adapter;
    ArrayList<String> lista = new ArrayList<>();
    Integer num1, num2, num3, num4;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_secreto);
        codigo = findViewById(R.id.CodigoSecreto);
        texto = findViewById(R.id.EdtCodigo);
        btnCodigo = findViewById(R.id.btnCodigo);
        btnVolta = findViewById(R.id.btnVoltaMenu);
        listView = findViewById(R.id.ListaSecreta);
        testeSecreto = findViewById(R.id.testeSecreto);

        num1 = new Random().nextInt(9);
        num2 = new Random().nextInt(9);
        num3 = new Random().nextInt(9);
        num4 = new Random().nextInt(9);

        testeSecreto.setText(num1 +"-"+ num2 +"-"+ num3 +"-"+ num4);

        adapter = new ArrayAdapter(Secreto.this
                , android.R.layout.simple_list_item_1, lista);
        listView.setAdapter(adapter);

        //testeSecreto.setVisibility(View.INVISIBLE);

        btnCodigo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(texto.getText().toString().isEmpty()){
                    Toast.makeText(Secreto.this, "Campo não pode ser vazio", Toast.LENGTH_SHORT).show();
                }else {
                    String textofin = "";

                    texto.getText().toString().charAt(0);

                    if(String.valueOf(texto.getText().toString().charAt(0)).equals(String.valueOf(num1))){
                        textofin=textofin+" Valor: "+texto.getText().toString().charAt(0)+", Correto |";
                    }
                    if(String.valueOf(texto.getText().toString().charAt(0)).equals(String.valueOf(num2))
                            || String.valueOf(texto.getText().toString().charAt(0)).equals(String.valueOf(num3))
                            || String.valueOf(texto.getText().toString().charAt(0)).equals(String.valueOf(num4))){
                        textofin =textofin+ " Valor: "+texto.getText().toString().charAt(0)+", Posição errada |";
                    }else if(!String.valueOf(texto.getText().charAt(0)).equals(String.valueOf(num1)) &&
                            !String.valueOf(texto.getText().charAt(1)).equals(String.valueOf(num2)) &&
                            !String.valueOf(texto.getText().charAt(2)).equals(String.valueOf(num3)) &&
                            !String.valueOf(texto.getText().charAt(3)).equals(String.valueOf(num4))){
                        textofin =textofin+ " Valor: "+texto.getText().toString().charAt(0)+", Errado |";
                    }

                    //

                    if((String.valueOf(texto.getText().toString().charAt(1)).equals(String.valueOf(num2)))){
                        textofin=textofin+" Valor: "+texto.getText().toString().charAt(1)+", Correto |";
                    }
                    if((String.valueOf(texto.getText().toString().charAt(1)).equals(String.valueOf(num1)))
                            || (String.valueOf(texto.getText().toString().charAt(1)).equals(String.valueOf(num3)))
                            || (String.valueOf(texto.getText().toString().charAt(1)).equals(String.valueOf(num4)))){
                        textofin =textofin+ " Valor: "+texto.getText().toString().charAt(1)+", Posição errada |";
                    }else if (!String.valueOf(texto.getText().charAt(0)).equals(String.valueOf(num1)) &&
                            !String.valueOf(texto.getText().charAt(1)).equals(String.valueOf(num2)) &&
                            !String.valueOf(texto.getText().charAt(2)).equals(String.valueOf(num3)) &&
                            !String.valueOf(texto.getText().charAt(3)).equals(String.valueOf(num4))) {
                        textofin =textofin+ " Valor: "+texto.getText().toString().charAt(1)+", Errado |";
                    }

                    //

                    if((String.valueOf(texto.getText().toString().charAt(2)).equals(String.valueOf(num3)))){
                        textofin=textofin+" Valor: "+texto.getText().toString().charAt(2)+", Correto |";
                    }
                    if((String.valueOf(texto.getText().toString().charAt(2)).equals(String.valueOf(num2)))
                            || (String.valueOf(texto.getText().toString().charAt(2)).equals(String.valueOf(num1)))
                            || (String.valueOf(texto.getText().toString().charAt(2)).equals(String.valueOf(num4)))){
                        textofin =textofin+ " Valor: "+texto.getText().toString().charAt(2)+", Posição errada |";
                    }else if(!String.valueOf(texto.getText().charAt(0)).equals(String.valueOf(num1)) &&
                            !String.valueOf(texto.getText().charAt(1)).equals(String.valueOf(num2)) &&
                            !String.valueOf(texto.getText().charAt(2)).equals(String.valueOf(num3)) &&
                            !String.valueOf(texto.getText().charAt(3)).equals(String.valueOf(num4))) {
                        textofin =textofin+ " Valor: "+texto.getText().toString().charAt(2)+", Errado |";
                    }

                    //

                    if(String.valueOf(texto.getText().charAt(3)).equals(String.valueOf(num4))){
                        textofin=textofin+" Valor: "+texto.getText().toString().charAt(3)+", Correto |";
                    }
                    if(String.valueOf(texto.getText().charAt(3)).equals(String.valueOf(num2))
                            || String.valueOf(texto.getText().charAt(3)).equals(String.valueOf(num3))
                            || String.valueOf(texto.getText().charAt(3)).equals(String.valueOf(num1))){
                        textofin =textofin+ " Valor: "+texto.getText().toString().charAt(3)+", Posição errada |";
                    }else if(!String.valueOf(texto.getText().charAt(0)).equals(String.valueOf(num1)) &&
                            !String.valueOf(texto.getText().charAt(1)).equals(String.valueOf(num2)) &&
                            !String.valueOf(texto.getText().charAt(2)).equals(String.valueOf(num3)) &&
                            !String.valueOf(texto.getText().charAt(3)).equals(String.valueOf(num4))) {
                        textofin =textofin+ " Valor: "+texto.getText().toString().charAt(3)+", Errado |";
                    }

                    //

                    if(String.valueOf(texto.getText().charAt(0)).equals(String.valueOf(num1)) &&
                            String.valueOf(texto.getText().charAt(1)).equals(String.valueOf(num2)) &&
                            String.valueOf(texto.getText().charAt(2)).equals(String.valueOf(num3)) &&
                            String.valueOf(texto.getText().charAt(3)).equals(String.valueOf(num4))){
                        codigo.setText(" "+num1+" - "+num2+" - "+num3+" - "+num4+" ");
                        Toast.makeText(Secreto.this, "Parebéns, Voçê acertou o Codigo secreto" +
                                "  Para Reiniciar saia e volte! ", Toast.LENGTH_LONG).show();
                    }



                    lista.add(textofin);
                    texto.setText("");
                    adapter.notifyDataSetChanged();
                    listView.setAdapter(adapter);

                }
            }
        });

        /*View.OnLongClickListener listener = new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                Button clickedButton = (Button) view;
                testeSecreto.setVisibility(View.VISIBLE);
                return true;
            }
        };

        btnCodigo.setOnLongClickListener(listener);*/

        btnVolta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Secreto.this, Tela2.class);
                startActivity(i);
                finish();
            }
        });

    }
}