package com.example.atividadeavaliativa;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Tela2 extends AppCompatActivity {

    Button btnInverter, btnSecreto, btnPedra, btnRandom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela2);
        btnPedra = findViewById(R.id.btnPedra);
        btnInverter = findViewById(R.id.btnInversor);
        btnRandom = findViewById(R.id.btnRandom);
        btnSecreto = findViewById(R.id.btnSecreto);

        btnInverter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Tela2.this, Inversor.class);
                startActivity(i);
                finish();
            }
        });

        btnPedra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Tela2.this, PPT.class);
                startActivity(i);
                finish();
            }
        });

        btnRandom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Tela2.this, Randomizador.class);
                startActivity(i);
                finish();
            }
        });

        btnSecreto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Tela2.this, Secreto.class);
                startActivity(i);
                finish();
            }
        });

    }
}