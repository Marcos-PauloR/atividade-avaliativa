package com.example.atividadeavaliativa;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

public class Randomizador extends AppCompatActivity {
    Button add, volta, aleatorio;
    EditText edt;
    TextView textView;
    ListView listView;
    ArrayAdapter adapter;
    ArrayList<String> lista = new ArrayList<String>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_randomizador);
        add = findViewById(R.id.AddFruta);
        volta = findViewById(R.id.VoltaTela2);
        edt = findViewById(R.id.Fruta);
        textView = findViewById(R.id.FrutaEscolhida);
        listView = findViewById(R.id.ListaFrutas);
        aleatorio = findViewById(R.id.Aleatoriza);

        adapter = new ArrayAdapter(Randomizador.this
                , android.R.layout.simple_list_item_1, lista);
        listView.setAdapter(adapter);


        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(edt.getText().toString().isEmpty()){
                    Toast.makeText(Randomizador.this, "Campo não pode ser vazio", Toast.LENGTH_SHORT).show();
                }else {
                    lista.add(edt.getText().toString());
                    Toast.makeText(Randomizador.this, "Fruta: "+edt.getText().toString()+ " Adicionada!", Toast.LENGTH_SHORT).show();
                    edt.setText("");
                    adapter.notifyDataSetChanged();
                    listView.setAdapter(adapter);

                }
            }
        });

        aleatorio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int r = new Random().nextInt(lista.size());
                textView.setText(adapter.getItem(r).toString());
            }
        });

        volta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Randomizador.this, Tela2.class);
                startActivity(i);
                finish();
            }
        });



    }
}