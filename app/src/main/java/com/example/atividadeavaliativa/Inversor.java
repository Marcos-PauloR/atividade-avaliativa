package com.example.atividadeavaliativa;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Inversor extends AppCompatActivity {

    Button btn, btnV;
    EditText edt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inversor);
        btn= findViewById(R.id.btnTelaInvertido);
        edt = findViewById(R.id.edtInverte);
        btnV = findViewById(R.id.btnVolta2);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Inversor.this, TelaInvertida.class);
                String string = new StringBuffer(edt.getText().toString()).reverse().toString() ;
                i.putExtra("PalavraInvertida", string);
                startActivity(i);
            }
        });

        btnV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Inversor.this, Tela2.class);
                startActivity(i);
                finish();
            }
        });


    }
}