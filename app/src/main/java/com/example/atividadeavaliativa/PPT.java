package com.example.atividadeavaliativa;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Random;

public class PPT extends AppCompatActivity {
    ImageButton pedra, papel, tesoura;
    Button btn;
    TextView status;
    ImageView img;
    int valor = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ppt);

        pedra = findViewById(R.id.btnPedra);
        papel = findViewById(R.id.btnPapel);
        tesoura = findViewById(R.id.btnTesoura);
        status = findViewById(R.id.Status);
        img = findViewById(R.id.imgInimigo);
        btn = findViewById(R.id.btnVolta2);

        //Valor = 0 (Pedra)
        //Valor = 1 (Papel)
        //Valor = 2 (Tesoura)

        pedra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                valor = new Random().nextInt(3);
                if( valor == 0 ){
                    img.setImageResource(R.mipmap.pedra);
                    status.setText("Você Empatou!");
                }else if (valor == 1){
                    img.setImageResource(R.mipmap.papel);
                    status.setText("Você Perdeu!");
                }else {
                    img.setImageResource(R.mipmap.tesoura);
                    status.setText("Você Ganhou!");
                }
            }
        });

        papel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                valor = new Random().nextInt(3);
                if( valor == 0 ){
                    img.setImageResource(R.mipmap.pedra);
                    status.setText("Você Ganhou!");
                }else if (valor == 1){
                    img.setImageResource(R.mipmap.papel);
                    status.setText("Você Empatou!");
                }else {
                    img.setImageResource(R.mipmap.tesoura);
                    status.setText("Você Perdeu!");
                }
            }
        });

        tesoura.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                valor = new Random().nextInt(3);
                if( valor == 0 ){
                    img.setImageResource(R.mipmap.pedra);
                    status.setText("Você Perdeu!");
                }else if (valor == 1){
                    img.setImageResource(R.mipmap.papel);
                    status.setText("Você Ganhou!");
                }else {
                    img.setImageResource(R.mipmap.tesoura);
                    status.setText("Você Empatou!");
                }
            }
        });

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(PPT.this, Tela2.class);
                startActivity(i);
                finish();
            }
        });


    }
}